package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;


public class DBManager {



	private static DBManager instance;
	private static DBManager dbManager;

	//private static String CONNECTION_URL = "jdbc:mysql://localhost:3306/test2db?user=root&password=password";
	private static String CONNECTION_URL = "jdbc:derby:memory:testdb;create=true";
	//private static String CONNECTION_URL = "jdbc:mysql://10.7.0.9:3306/testdb?user=testuser&password=testpass";
	//private static String CONNECTION_URL = "jdbc:mysql:D:\\JAVA\\EPAM\\Modul2\\Task_07/testdb?user=root&password=password";
	private static final String SQL_CREATE_TEAM = "INSERT INTO teams VALUES (default, ?)";
	private static final String SQL_FIND_ALL_USERS = "select * from users";
	private static final String SQL_CREATE_USER = "insert into users values (default, ?)";
	private static final String SQL_FIND_USER_BY_LOGIN = "SELECT id, login FROM users WHERE login = ?";
	private static final String SQL_FIND_TEAM_BY_NAME = "SELECT id, name FROM teams WHERE name = ?";
	//private static final String INSERT_USERS = "INSERT INTO users (login) VALUES (?)";
	private static final String SQL_DELETE_USER = "delete from users where id=?";
	private static final String SQL_FIND_ALL_TEAMS = "SELECT * FROM teams";
	private static final String SQL_UPDATE_TEAM = "UPDATE teams SET name = ? WHERE id = ?";
	private static final String SQL_SET_TEAMS_FOR_USER = "INSERT INTO users_teams VALUES (?, ?)";
	private static final String SQL_SELECT_TEAM_BY_USER = "SELECT u.login, t.name FROM users u " +
			"JOIN users_teams ON u.id = users_teams.user_id JOIN teams t ON t.id=users_teams.team_id " +
			"WHERE u.login = ?";
	private static final String SQL_DELETE_USERS = "delete from users where id=?";
	private static final String SQL_DELETE_TEAM = "DELETE FROM teams WHERE id=?";


	public static synchronized DBManager getInstance() {
		if (instance == null) {
			instance = new DBManager();
		}
		return instance;
	}

	private DBManager() {
	}

	public List<User> findAllUsers() throws DBException {
		List<User> users = new ArrayList<>();
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL_FIND_ALL_USERS);
			while (rs.next()) {
				users.add(extractUser(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(con);
			closeResultSet(rs);
		}
		return users;
	}

	private User extractUser(ResultSet rs) throws SQLException {
		User user = User.createUser(rs.getString("login"));
		user.setId(rs.getInt("id"));
		//user.setLogin(rs.getString("login"));
		return user;
	}

	public void insertUser(User user) throws DBException {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;

		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_CREATE_USER, Statement.RETURN_GENERATED_KEYS); //
			int k = 1;
			pstmt.setString(k++, user.getLogin());

			if (pstmt.executeUpdate() > 0) {
				rs = pstmt.getGeneratedKeys();
				if (rs.next()) {
					user.setId(rs.getInt(1));
				}
//				DatabaseMetaData databaseMetaData = con.getMetaData();
//				System.out.println(databaseMetaData.getUserName());
//				System.out.println(databaseMetaData.getURL());
//				System.out.println(databaseMetaData.getDatabaseProductName());
//				System.out.println(databaseMetaData.getDatabaseProductVersion());
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(con);
			closeResultSet(rs);
		}
	}

	private void close(Connection con) {
		if (con != null) {
			try {
				con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	private static void rollback(Connection con) {
		try {
			if (con != null) {
				con.rollback();
			}
		} catch (SQLException e) {
			// just write to log
			e.printStackTrace();
		}
	}

	public Connection getConnection() throws SQLException {
		Connection con = DriverManager.getConnection(CONNECTION_URL);
		return con;
	}

	public static void closeResultSet(ResultSet resultSet) {
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException throwables) {
				System.err.println(throwables.getMessage());
			}
		}
	}

	public boolean deleteUsers(User... users) throws DBException {
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			pstm = con.prepareStatement(SQL_DELETE_USERS, Statement.RETURN_GENERATED_KEYS);
			for (User user : users) {
				pstm.setInt(1, user.getId());
				//pstm.setString(2, user.getLogin());
				pstm.executeUpdate();
			}
			con.commit();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			close(con);
			closeResultSet(rs);
		}

		return false;
	}

	public User getUser(String login) {
		User user = null;
		Connection con = null;
		PreparedStatement pstm = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			pstm = con.prepareStatement(SQL_FIND_USER_BY_LOGIN);
			pstm.setString(1, login);
			rs = pstm.executeQuery();
			if (rs.next()) {
//				user = User.createUser(rs.getString("login"));
//				user.setId(rs.getInt("id"));
				user = extractUser(rs);
			}

		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			close(con);
			closeResultSet(rs);
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		Team team = new Team();
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_FIND_TEAM_BY_NAME);
			pstmt.setString(1, name);
			rs = pstmt.executeQuery();
			if (rs.next()) {
//				team = Team.createTeam(rs.getString("name"));
//				team.setId(rs.getInt("id"));
				team = getTeam(rs);
			}
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			close(con);
			closeResultSet(rs);
		}

		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		List<Team> teams = new ArrayList<>();
		Connection con = null;
		Statement stmt = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			stmt = con.createStatement();
			rs = stmt.executeQuery(SQL_FIND_ALL_TEAMS);
			while (rs.next()) {
				teams.add(getTeam(rs));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(con);
			closeResultSet(rs);
		}
		return teams;
	}

	private Team getTeam(ResultSet rs) throws SQLException {
		Team t = Team.createTeam(rs.getString("name"));
		t.setId(rs.getInt("id"));
		//t.setName(rs.getString("name"));
		return t;
	}

	public void insertTeam(Team team) throws DBException {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_CREATE_TEAM, Statement.RETURN_GENERATED_KEYS);
			int k = 1;
			pstmt.setString(k++, team.getName());

			if (pstmt.executeUpdate() > 0) {
				rs = pstmt.getGeneratedKeys();
				if (rs.next()) {
					team.setId(rs.getInt(1));
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(con);
		}
	}

	public void setTeamsForUser(User user, Team... teams) throws DBException {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = getConnection();
			con.setAutoCommit(false);
			pstmt = con.prepareStatement(SQL_SET_TEAMS_FOR_USER);
			for (int i = 0; i < teams.length; i++) {
				pstmt.setInt(1, user.getId());
				pstmt.setInt(2, teams[i].getId());
				pstmt.executeUpdate();
			}
			con.commit();
		} catch (SQLException ex) {
			ex.printStackTrace();
			rollback(con);
			throw new DBException("Cannot set Team", ex);
		} finally {
			close(con);
		}

	}

	public List<Team> getUserTeams(User user) throws DBException {
		Connection con = null;
		PreparedStatement pstmt = null;
		ResultSet rs = null;
		List<Team> list = new ArrayList<>();
		Team team = new Team();
		try {
			con = getConnection();
			con.setAutoCommit(false);
			pstmt = con.prepareStatement(SQL_SELECT_TEAM_BY_USER);
			pstmt.setString(1, user.getLogin());
			rs = pstmt.executeQuery();
			while (rs.next()) {
				team = Team.createTeam(rs.getString("name"));
				list.add(team);
			}
			con.commit();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			closeResultSet(rs);
			close(con);
		}
		return list;
	}

	public boolean deleteTeam(Team team) throws DBException {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_DELETE_TEAM);
			pstmt.setInt(1, team.getId());
			pstmt.executeUpdate();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			close(con);
		}
		return false;
	}

	public void updateTeam(Team team) throws DBException {
		Connection con = null;
		PreparedStatement pstmt = null;
		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_UPDATE_TEAM, Statement.RETURN_GENERATED_KEYS);
			int k = 1;
			pstmt.setString(k++, team.getName());
			pstmt.setInt(k++, team.getId());
			pstmt.executeUpdate();
		} catch (SQLException ex) {
			ex.printStackTrace();
		} finally {
			close(con);
		}
	}

	public void deleteUser(int userId) {
		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = getConnection();
			pstmt = con.prepareStatement(SQL_DELETE_USER);
			int k = 1;
			pstmt.setInt(k++, userId);

			pstmt.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close(con);
		}
	}



}
