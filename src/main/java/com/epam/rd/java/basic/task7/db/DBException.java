package com.epam.rd.java.basic.task7.db;

import java.sql.SQLException;

public class DBException extends Exception {

	public DBException(String message, Throwable cause) {
		super(message, cause);
	}
}
